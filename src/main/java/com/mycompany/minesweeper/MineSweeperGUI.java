package com.mycompany.minesweeper;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet.ColorAttribute;

public class MineSweeperGUI extends JPanel implements MouseListener {

    private JButton[][] spielfeld = new JButton[10][10];

    MineSweeperLogik MSL = new MineSweeperLogik();

    //int kor1;
    //int kor2;
    public MineSweeperGUI() {
        setLayout(null);

        int h = 10;

        for (int i = 0; i < spielfeld.length; i++) {

            int w = 20;
            for (int j = 0; j < spielfeld.length; j++) {

                spielfeld[i][j] = new JButton();
                spielfeld[i][j].setBounds(w, h, 60, 60);
                spielfeld[i][j].setBackground(Color.white);
                spielfeld[i][j].setOpaque(true);
                spielfeld[i][j].setBorder(BorderFactory.createLineBorder(Color.black, 2));
                spielfeld[i][j].addMouseListener(this);
                this.add(spielfeld[i][j]);
                w = w + 70;
            }
            h = h + 70;
        }

        MSL.bombaAusgeben();
    }

    private void unfoldButtons(int i, int j) {

        Vector<JButton> buttons = new Vector<>();
        buttons.add(spielfeld[i][j]);

        while (!buttons.isEmpty()) {

            JButton button = buttons.remove(0);
            int a = 0;
            int b = 0;
            for (int k = 0; k < spielfeld.length; k++) {
                for (int h = 0; h < spielfeld.length; h++) {
                    if (spielfeld[k][h].equals(button)) {
                        a = k;
                        b = h;
                    }
                }
            }

            int zahl = MSL.wert(a, b);
            button.setText(zahl + "");
            button.setEnabled(false);

            if (zahl == 0) {
                if (a > 0 && spielfeld[a - 1][b].isEnabled() && !buttons.contains(spielfeld[a - 1][b])) {
                    buttons.add(spielfeld[a - 1][b]);
                }
                if (b > 0 && spielfeld[a][b - 1].isEnabled() && !buttons.contains(spielfeld[a][b - 1])) {
                    buttons.add(spielfeld[a][b - 1]);
                }
                if (a < 9 && spielfeld[a + 1][b].isEnabled() && !buttons.contains((spielfeld[a + 1][b]))) {
                    buttons.add(spielfeld[a + 1][b]);
                }
                if (b < 9 && spielfeld[a][b + 1].isEnabled() && !buttons.contains((spielfeld[a][b + 1]))) {
                    buttons.add(spielfeld[a][b + 1]);
                }
                if (a > 0 && b > 0 && spielfeld[a - 1][b - 1].isEnabled() && !buttons.contains(spielfeld[a - 1][b - 1])) {
                    buttons.add(spielfeld[a - 1][b - 1]);
                }
                if (a < 9 && b < 9 && spielfeld[a + 1][b + 1].isEnabled() && !buttons.contains((spielfeld[a + 1][b + 1]))) {
                    buttons.add(spielfeld[a + 1][b + 1]);
                }
                if (a < 9 && b > 0 && spielfeld[a + 1][b - 1].isEnabled() && !buttons.contains(spielfeld[a + 1][b - 1])) {
                    buttons.add(spielfeld[a + 1][b - 1]);
                }
                if (a > 0 && b < 9 && spielfeld[a - 1][b + 1].isEnabled() && !buttons.contains(spielfeld[a - 1][b + 1])) {
                    buttons.add(spielfeld[a - 1][b + 1]);
                }

            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            for (int i = 0; i < spielfeld.length; i++) {
                for (int j = 0; j < spielfeld.length; j++) {
                    if (e.getSource().equals(spielfeld[i][j])) {
                        spielfeld[i][j].setFocusable(false);
                        unfoldButtons(i, j);
                        if (MSL.überprüfenBomba(i, j) == false) {
                            MSL.bombenAufdecken(spielfeld);
                            JOptionPane.showMessageDialog(null, "Du hast verloren!", "Schade!", JOptionPane.PLAIN_MESSAGE);
                        }

                    }
                }
            }
        } else if (SwingUtilities.isRightMouseButton(e)) {
            for (int i = 0; i < spielfeld.length; i++) {
                for (int j = 0; j < spielfeld.length; j++) {
                    if (e.getSource().equals(spielfeld[i][j])) {
                        if (spielfeld[i][j].getText().equals("X")) {
                            spielfeld[i][j].setText("");
                            spielfeld[i][j].setBackground(Color.white);
                            spielfeld[i][j].setEnabled(true);
                            spielfeld[i][j].setOpaque(true);
                        } else {
                            spielfeld[i][j].setText("X");
                            spielfeld[i][j].setEnabled(false);
                            spielfeld[i][j].setBackground(Color.green);
                            spielfeld[i][j].setOpaque(true);
                            if (MSL.hatGewonnen(spielfeld)) {
                                JOptionPane.showMessageDialog(null, "Du hast gewonnen!", "Glückwunsch!", JOptionPane.PLAIN_MESSAGE);
                            }
                        }

                    }
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld.length; j++) {
                if (e.getSource().equals(spielfeld[i][j]) && spielfeld[i][j].getText() != "X"
                        && e.getSource().equals(spielfeld[i][j]) && spielfeld[i][j].getText() != "-1") {
                    spielfeld[i][j].setBackground(Color.lightGray);

                }
            }

        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld.length; j++) {
                if (e.getSource().equals(spielfeld[i][j]) && spielfeld[i][j].getText() != "X"
                        && e.getSource().equals(spielfeld[i][j]) && spielfeld[i][j].getText() != "-1") {
                    spielfeld[i][j].setBackground(Color.white);

                }
            }

        }
    }

}
