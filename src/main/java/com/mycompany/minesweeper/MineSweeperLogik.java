package com.mycompany.minesweeper;

import java.awt.Color;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class MineSweeperLogik {

    private int[][] BOMBA;
    private int bombenAnzahl = 14;

    //MineSweeperGUI ohje = new MineSweeperGUI();

    public MineSweeperLogik() {
        boolean prüfen = true;
        BOMBA = new int[10][10];
        Random r = new Random();

        while (prüfen) {
            for (int i = 0; i < bombenAnzahl; i++) {
                int pos1 = r.nextInt(9) + 1;
                int pos2 = r.nextInt(9) + 1;

                if (BOMBA[pos1][pos2] != -1) {
                    BOMBA[pos1][pos2] = -1;
                } else {
                    i = i - 1;
                }
                if (i == bombenAnzahl - 1) {
                    prüfen = false;
                }
            }

        }

        for (int j = 0; j < BOMBA.length; j++) {
            for (int i = 0; i < BOMBA.length; i++) {
                if (0 == BOMBA[i][j]) {
                    if (i - 1 >= 0) {
                        if (BOMBA[i - 1][j] == -1) {

                            BOMBA[i][j] = BOMBA[i][j] + 1;
                        }
                    }
                    if (i + 1 <= 9) {
                        if (BOMBA[i + 1][j] == -1) {

                            BOMBA[i][j] = BOMBA[i][j] + 1;
                        }
                    }
                    if (j - 1 >= 0) {
                        if (BOMBA[i][j - 1] == -1) {

                            BOMBA[i][j] = BOMBA[i][j] + 1;
                        }
                    }
                    if (j + 1 <= 9) {
                        if (BOMBA[i][j + 1] == -1) {

                            BOMBA[i][j] = BOMBA[i][j] + 1;
                        }
                    }
                    if (i - 1 >= 0 && j - 1 >= 0) {
                        if (BOMBA[i - 1][j - 1] == -1) {

                            BOMBA[i][j] = BOMBA[i][j] + 1;
                        }
                    }
                    if (i + 1 <= 9 && j - 1 >= 0) {
                        if (BOMBA[i + 1][j - 1] == -1) {

                            BOMBA[i][j] = BOMBA[i][j] + 1;
                        }
                    }
                    if (j + 1 <= 9 && i - 1 >= 0) {

                        if (BOMBA[i - 1][j + 1] == -1) {

                            BOMBA[i][j] = BOMBA[i][j] + 1;
                        }
                    }
                    if (j + 1 <= 9 && i + 1 <= 9) {
                        if (BOMBA[i + 1][j + 1] == -1) {

                            BOMBA[i][j] = BOMBA[i][j] + 1;
                        }
                    }

                }

            }

        }

    }

    public void bombaAusgeben() {
        for (int i = 0; i < BOMBA.length; i++) {
            for (int j = 0; j < BOMBA.length; j++) {
                if (BOMBA[i][j] == -1) {
                    System.out.print(BOMBA[i][j]);
                    System.out.print(" | ");
                } else {
                    System.out.print(BOMBA[i][j]);
                    System.out.print("  | ");
                }

            }

            System.out.println();

        }
    }

    public boolean hatGewonnen(JButton[][] spielfeld) {
        int anzahlBombe = 0;
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld.length; j++) {
                if (spielfeld[i][j].getText().equals("X") && BOMBA[i][j] == -1) {
                    anzahlBombe = anzahlBombe + 1;
                }
            }
        }
        if (anzahlBombe == bombenAnzahl) {
            return true;
        }else{
            return false;
        }
    }

    public int wert(int i, int j) {
        return BOMBA[i][j];
    }

    public boolean überprüfenBomba(int kor1, int kor2) {
        if (BOMBA[kor1][kor2] == -1) {
            return false;
        } else {
            return true;
        }
    }
    
    public void bombenAufdecken(JButton spielfeld[][]){
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld.length; j++) {
                if(BOMBA[i][j]==-1){
                    spielfeld[i][j].setText("-1");
                    spielfeld[i][j].setBackground(Color.red);
                    spielfeld[i][j].setOpaque(true);
                }
                
            }
            
        }
    }

    public boolean überprüfenNull(int kor1, int kor2) {
        if (BOMBA[kor1][kor2] == 0) {
            return true;
        } else {
            return false;
        }
    }

    public String UpLeft(int kor1, int kor2) {
        return BOMBA[kor1 - 1][kor2 - 1] + "";
    }

    public String UpMid(int kor1, int kor2) {
        return BOMBA[kor1 - 1][kor2] + "";
    }

    public String UpRight(int kor1, int kor2) {
        return BOMBA[kor1 - 1][kor2 + 1] + "";
    }

    public String DownLeft(int kor1, int kor2) {
        return BOMBA[kor1 + 1][kor2 - 1] + "";
    }

    public String DownMid(int kor1, int kor2) {
        return BOMBA[kor1 + 1][kor2] + "";
    }

    public String DownRight(int kor1, int kor2) {
        return BOMBA[kor1 + 1][kor2 + 1] + "";
    }

    public String LeftMid(int kor1, int kor2) {
        return BOMBA[kor1][kor2 - 1] + "";
    }

    public String RightMid(int kor1, int kor2) {
        return BOMBA[kor1][kor2 + 1] + "";
    }

}
