package com.mycompany.minesweeper;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

public class MineSweeper {

    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(750, 750);
        frame.setTitle("  Minesweeper");
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        
        UIManager.put("Button.disabledText", new ColorUIResource(Color.BLACK));
        
        
        
        MineSweeperGUI gui = new MineSweeperGUI();
        
        frame.setContentPane(gui);
        
        frame.getContentPane().setBackground(Color.gray);
        
        ImageIcon icon = new ImageIcon("icon.png");
        frame.setIconImage(icon.getImage());

        frame.setVisible(true);
        
    }
}
